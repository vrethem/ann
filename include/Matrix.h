/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#ifndef _MATRIX_H_
#define _MATRIX_H_

//=================================  
// Constants

//=================================  
// forward declared dependencies

//=================================  
// included dependencies
#include <vector>
#include <random>
#include <initializer_list>
//=================================  
// class Layer
class Matrix
{ 
public:
   Matrix(int rows, int columns, bool randomize_content = false);
   Matrix(std::initializer_list< std::vector<double> > list);
   Matrix(int rows, int columns, std::initializer_list<double> list);
   Matrix(Matrix* );  

   Matrix *copy();
   Matrix* getTranspose();
   void transpose(); 
   
   bool inRange(int row, int column) const;

//Setters
   void setVal( int row, int column, double val);
//Getters
   double getVal(int row, int column) const;
   int getRows()   const  {return rows;}
   int getColumns()const  {return columns;}
   double getRandomSigmoid();

   void printToConsole() const;
private:
   int rows;
   int columns;

   std::vector< std::vector<double> > matrix; 
}; 

#endif // __MATRIX_H_


/*
if (rows == columns) {
      for (vector<vector<double>>::iterator r{matrix.begin()}; r != matrix.end();r++) {
         for (vector<double>::iterator c{r->begin()}; c != r->end(); c++} //int c{r+1}; c < columns; c++) {
         swap(matrix[distance(matrix.begin(),r)][distance(); 
         // Problem because swap(matrix[r][c], matrix[c][r]) cant be done when iterater r is a vector... 
         }
      }
   }
*/
