/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#ifndef _LAYER_H_
#define _LAYER_H_

//=================================  
// Constants

//=================================  
// forward declared dependencies
class Neuron;
class Matrix;

//=================================  
// included dependencies
#include <vector>


//=================================  
// class Layer
class Layer
{
public: 
   Layer(int size);
   Layer(int size, int activationType);

   Matrix* matrixify(); 
   Matrix* matrixifyActivatedVals();
   Matrix* matrixifyDerivedVals();

//Getters   
   std::vector<double> getActivatedVals();
   std::vector<Neuron *> getNeurons();

//Setters
   void setNeurons(std::vector<Neuron *> neurons);
   void setVal(int i, double v);
private:
   int size;
   std::vector<Neuron *> neurons;
}; 

#endif // __LAYER_H_
