/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#ifndef _NEURON_H_
#define _NEURON_H_

//=================================  
// forward declared dependencies

//=================================  
// included dependencies

//=================================  
/**Fast sigmoid function
   f(x) = x / (1 + |x|)
   Derivative for fast sigmoid function
   f'(x) = f(x) * (1 - f(x))     **/
//=================================  
// class Neuron
class Neuron
{
public:
   Neuron(double val);
   Neuron(double val, int activationType);
  
   void activate(); // Activate neuron
   void derive();   // Derive neuron 
   
   //Setter
   void setVal(double& v);
   //Getter 
   double getVal()         const {return this->val;}
   double getActivatedVal()const {return this->activatedVal;}
   double getDerivedVal()  const {return this->derivedVal;}
  
private:
   double val;
   double activatedVal;

   double derivedVal;
   int activationType = 3;
}; 

#endif // __NEURON_H_
