/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#ifndef _ANN_H_
#define _ANN_H_

//=================================  
// Constants

//=================================  
// forward declared dependencies
class Matrix;
class Layer;

//=================================  
// included dependencies
#include <vector>

//=================================  
// class Artificial Neural Network
class ANN
{ 
public:
   ANN(std::vector<int> topology);
   void setCurrentInput(std::vector<double> input);
   void print();
private:
   int                  topologySize;
   std::vector<int>     topology;
   std::vector<Layer*>  layers;
   std::vector<Matrix*> weightMatrixes;
   std::vector<double>  input;
}; 

#endif // __ANN_H_
 
