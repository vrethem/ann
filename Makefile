# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/d0rsha/Skrivbord/ann

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/d0rsha/Skrivbord/ann

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/cmake-gui -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/d0rsha/Skrivbord/ann/CMakeFiles /home/d0rsha/Skrivbord/ann/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/d0rsha/Skrivbord/ann/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named zap

# Build rule for target.
zap: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 zap
.PHONY : zap

# fast build rule for target.
zap/fast:
	$(MAKE) -f CMakeFiles/zap.dir/build.make CMakeFiles/zap.dir/build
.PHONY : zap/fast

#=============================================================================
# Target rules for targets named main

# Build rule for target.
main: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 main
.PHONY : main

# fast build rule for target.
main/fast:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/build
.PHONY : main/fast

#=============================================================================
# Target rules for targets named test_matrix

# Build rule for target.
test_matrix: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 test_matrix
.PHONY : test_matrix

# fast build rule for target.
test_matrix/fast:
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/build
.PHONY : test_matrix/fast

src/ANN.o: src/ANN.cc.o

.PHONY : src/ANN.o

# target to build an object file
src/ANN.cc.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/ANN.cc.o
.PHONY : src/ANN.cc.o

src/ANN.i: src/ANN.cc.i

.PHONY : src/ANN.i

# target to preprocess a source file
src/ANN.cc.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/ANN.cc.i
.PHONY : src/ANN.cc.i

src/ANN.s: src/ANN.cc.s

.PHONY : src/ANN.s

# target to generate assembly for a file
src/ANN.cc.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/ANN.cc.s
.PHONY : src/ANN.cc.s

src/Layer.o: src/Layer.cc.o

.PHONY : src/Layer.o

# target to build an object file
src/Layer.cc.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Layer.cc.o
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Layer.cc.o
.PHONY : src/Layer.cc.o

src/Layer.i: src/Layer.cc.i

.PHONY : src/Layer.i

# target to preprocess a source file
src/Layer.cc.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Layer.cc.i
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Layer.cc.i
.PHONY : src/Layer.cc.i

src/Layer.s: src/Layer.cc.s

.PHONY : src/Layer.s

# target to generate assembly for a file
src/Layer.cc.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Layer.cc.s
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Layer.cc.s
.PHONY : src/Layer.cc.s

src/Matrix.o: src/Matrix.cc.o

.PHONY : src/Matrix.o

# target to build an object file
src/Matrix.cc.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Matrix.cc.o
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Matrix.cc.o
.PHONY : src/Matrix.cc.o

src/Matrix.i: src/Matrix.cc.i

.PHONY : src/Matrix.i

# target to preprocess a source file
src/Matrix.cc.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Matrix.cc.i
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Matrix.cc.i
.PHONY : src/Matrix.cc.i

src/Matrix.s: src/Matrix.cc.s

.PHONY : src/Matrix.s

# target to generate assembly for a file
src/Matrix.cc.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Matrix.cc.s
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Matrix.cc.s
.PHONY : src/Matrix.cc.s

src/NeuralNetwork/NeuralNetwork.o: src/NeuralNetwork/NeuralNetwork.cpp.o

.PHONY : src/NeuralNetwork/NeuralNetwork.o

# target to build an object file
src/NeuralNetwork/NeuralNetwork.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/NeuralNetwork.cpp.o
.PHONY : src/NeuralNetwork/NeuralNetwork.cpp.o

src/NeuralNetwork/NeuralNetwork.i: src/NeuralNetwork/NeuralNetwork.cpp.i

.PHONY : src/NeuralNetwork/NeuralNetwork.i

# target to preprocess a source file
src/NeuralNetwork/NeuralNetwork.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/NeuralNetwork.cpp.i
.PHONY : src/NeuralNetwork/NeuralNetwork.cpp.i

src/NeuralNetwork/NeuralNetwork.s: src/NeuralNetwork/NeuralNetwork.cpp.s

.PHONY : src/NeuralNetwork/NeuralNetwork.s

# target to generate assembly for a file
src/NeuralNetwork/NeuralNetwork.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/NeuralNetwork.cpp.s
.PHONY : src/NeuralNetwork/NeuralNetwork.cpp.s

src/NeuralNetwork/backPropagation.o: src/NeuralNetwork/backPropagation.cpp.o

.PHONY : src/NeuralNetwork/backPropagation.o

# target to build an object file
src/NeuralNetwork/backPropagation.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/backPropagation.cpp.o
.PHONY : src/NeuralNetwork/backPropagation.cpp.o

src/NeuralNetwork/backPropagation.i: src/NeuralNetwork/backPropagation.cpp.i

.PHONY : src/NeuralNetwork/backPropagation.i

# target to preprocess a source file
src/NeuralNetwork/backPropagation.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/backPropagation.cpp.i
.PHONY : src/NeuralNetwork/backPropagation.cpp.i

src/NeuralNetwork/backPropagation.s: src/NeuralNetwork/backPropagation.cpp.s

.PHONY : src/NeuralNetwork/backPropagation.s

# target to generate assembly for a file
src/NeuralNetwork/backPropagation.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/backPropagation.cpp.s
.PHONY : src/NeuralNetwork/backPropagation.cpp.s

src/NeuralNetwork/feedForward.o: src/NeuralNetwork/feedForward.cpp.o

.PHONY : src/NeuralNetwork/feedForward.o

# target to build an object file
src/NeuralNetwork/feedForward.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/feedForward.cpp.o
.PHONY : src/NeuralNetwork/feedForward.cpp.o

src/NeuralNetwork/feedForward.i: src/NeuralNetwork/feedForward.cpp.i

.PHONY : src/NeuralNetwork/feedForward.i

# target to preprocess a source file
src/NeuralNetwork/feedForward.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/feedForward.cpp.i
.PHONY : src/NeuralNetwork/feedForward.cpp.i

src/NeuralNetwork/feedForward.s: src/NeuralNetwork/feedForward.cpp.s

.PHONY : src/NeuralNetwork/feedForward.s

# target to generate assembly for a file
src/NeuralNetwork/feedForward.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/feedForward.cpp.s
.PHONY : src/NeuralNetwork/feedForward.cpp.s

src/NeuralNetwork/loadWeights.o: src/NeuralNetwork/loadWeights.cpp.o

.PHONY : src/NeuralNetwork/loadWeights.o

# target to build an object file
src/NeuralNetwork/loadWeights.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/loadWeights.cpp.o
.PHONY : src/NeuralNetwork/loadWeights.cpp.o

src/NeuralNetwork/loadWeights.i: src/NeuralNetwork/loadWeights.cpp.i

.PHONY : src/NeuralNetwork/loadWeights.i

# target to preprocess a source file
src/NeuralNetwork/loadWeights.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/loadWeights.cpp.i
.PHONY : src/NeuralNetwork/loadWeights.cpp.i

src/NeuralNetwork/loadWeights.s: src/NeuralNetwork/loadWeights.cpp.s

.PHONY : src/NeuralNetwork/loadWeights.s

# target to generate assembly for a file
src/NeuralNetwork/loadWeights.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/loadWeights.cpp.s
.PHONY : src/NeuralNetwork/loadWeights.cpp.s

src/NeuralNetwork/setErrors.o: src/NeuralNetwork/setErrors.cpp.o

.PHONY : src/NeuralNetwork/setErrors.o

# target to build an object file
src/NeuralNetwork/setErrors.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/setErrors.cpp.o
.PHONY : src/NeuralNetwork/setErrors.cpp.o

src/NeuralNetwork/setErrors.i: src/NeuralNetwork/setErrors.cpp.i

.PHONY : src/NeuralNetwork/setErrors.i

# target to preprocess a source file
src/NeuralNetwork/setErrors.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/setErrors.cpp.i
.PHONY : src/NeuralNetwork/setErrors.cpp.i

src/NeuralNetwork/setErrors.s: src/NeuralNetwork/setErrors.cpp.s

.PHONY : src/NeuralNetwork/setErrors.s

# target to generate assembly for a file
src/NeuralNetwork/setErrors.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/setErrors.cpp.s
.PHONY : src/NeuralNetwork/setErrors.cpp.s

src/NeuralNetwork/train.o: src/NeuralNetwork/train.cpp.o

.PHONY : src/NeuralNetwork/train.o

# target to build an object file
src/NeuralNetwork/train.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/train.cpp.o
.PHONY : src/NeuralNetwork/train.cpp.o

src/NeuralNetwork/train.i: src/NeuralNetwork/train.cpp.i

.PHONY : src/NeuralNetwork/train.i

# target to preprocess a source file
src/NeuralNetwork/train.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/train.cpp.i
.PHONY : src/NeuralNetwork/train.cpp.i

src/NeuralNetwork/train.s: src/NeuralNetwork/train.cpp.s

.PHONY : src/NeuralNetwork/train.s

# target to generate assembly for a file
src/NeuralNetwork/train.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/NeuralNetwork/train.cpp.s
.PHONY : src/NeuralNetwork/train.cpp.s

src/Neuron.o: src/Neuron.cc.o

.PHONY : src/Neuron.o

# target to build an object file
src/Neuron.cc.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Neuron.cc.o
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Neuron.cc.o
.PHONY : src/Neuron.cc.o

src/Neuron.i: src/Neuron.cc.i

.PHONY : src/Neuron.i

# target to preprocess a source file
src/Neuron.cc.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Neuron.cc.i
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Neuron.cc.i
.PHONY : src/Neuron.cc.i

src/Neuron.s: src/Neuron.cc.s

.PHONY : src/Neuron.s

# target to generate assembly for a file
src/Neuron.cc.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/Neuron.cc.s
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/Neuron.cc.s
.PHONY : src/Neuron.cc.s

src/main.o: src/main.cc.o

.PHONY : src/main.o

# target to build an object file
src/main.cc.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/main.cc.o
.PHONY : src/main.cc.o

src/main.i: src/main.cc.i

.PHONY : src/main.i

# target to preprocess a source file
src/main.cc.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/main.cc.i
.PHONY : src/main.cc.i

src/main.s: src/main.cc.s

.PHONY : src/main.s

# target to generate assembly for a file
src/main.cc.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/main.cc.s
.PHONY : src/main.cc.s

src/test_av_matris.o: src/test_av_matris.cc.o

.PHONY : src/test_av_matris.o

# target to build an object file
src/test_av_matris.cc.o:
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/test_av_matris.cc.o
.PHONY : src/test_av_matris.cc.o

src/test_av_matris.i: src/test_av_matris.cc.i

.PHONY : src/test_av_matris.i

# target to preprocess a source file
src/test_av_matris.cc.i:
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/test_av_matris.cc.i
.PHONY : src/test_av_matris.cc.i

src/test_av_matris.s: src/test_av_matris.cc.s

.PHONY : src/test_av_matris.s

# target to generate assembly for a file
src/test_av_matris.cc.s:
	$(MAKE) -f CMakeFiles/test_matrix.dir/build.make CMakeFiles/test_matrix.dir/src/test_av_matris.cc.s
.PHONY : src/test_av_matris.cc.s

src/utils/Math.o: src/utils/Math.cpp.o

.PHONY : src/utils/Math.o

# target to build an object file
src/utils/Math.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/Math.cpp.o
.PHONY : src/utils/Math.cpp.o

src/utils/Math.i: src/utils/Math.cpp.i

.PHONY : src/utils/Math.i

# target to preprocess a source file
src/utils/Math.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/Math.cpp.i
.PHONY : src/utils/Math.cpp.i

src/utils/Math.s: src/utils/Math.cpp.s

.PHONY : src/utils/Math.s

# target to generate assembly for a file
src/utils/Math.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/Math.cpp.s
.PHONY : src/utils/Math.cpp.s

src/utils/MatrixToVector.o: src/utils/MatrixToVector.cpp.o

.PHONY : src/utils/MatrixToVector.o

# target to build an object file
src/utils/MatrixToVector.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/MatrixToVector.cpp.o
.PHONY : src/utils/MatrixToVector.cpp.o

src/utils/MatrixToVector.i: src/utils/MatrixToVector.cpp.i

.PHONY : src/utils/MatrixToVector.i

# target to preprocess a source file
src/utils/MatrixToVector.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/MatrixToVector.cpp.i
.PHONY : src/utils/MatrixToVector.cpp.i

src/utils/MatrixToVector.s: src/utils/MatrixToVector.cpp.s

.PHONY : src/utils/MatrixToVector.s

# target to generate assembly for a file
src/utils/MatrixToVector.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/MatrixToVector.cpp.s
.PHONY : src/utils/MatrixToVector.cpp.s

src/utils/Misc.o: src/utils/Misc.cpp.o

.PHONY : src/utils/Misc.o

# target to build an object file
src/utils/Misc.cpp.o:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/Misc.cpp.o
.PHONY : src/utils/Misc.cpp.o

src/utils/Misc.i: src/utils/Misc.cpp.i

.PHONY : src/utils/Misc.i

# target to preprocess a source file
src/utils/Misc.cpp.i:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/Misc.cpp.i
.PHONY : src/utils/Misc.cpp.i

src/utils/Misc.s: src/utils/Misc.cpp.s

.PHONY : src/utils/Misc.s

# target to generate assembly for a file
src/utils/Misc.cpp.s:
	$(MAKE) -f CMakeFiles/main.dir/build.make CMakeFiles/main.dir/src/utils/Misc.cpp.s
.PHONY : src/utils/Misc.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... zap"
	@echo "... edit_cache"
	@echo "... main"
	@echo "... rebuild_cache"
	@echo "... test_matrix"
	@echo "... src/ANN.o"
	@echo "... src/ANN.i"
	@echo "... src/ANN.s"
	@echo "... src/Layer.o"
	@echo "... src/Layer.i"
	@echo "... src/Layer.s"
	@echo "... src/Matrix.o"
	@echo "... src/Matrix.i"
	@echo "... src/Matrix.s"
	@echo "... src/NeuralNetwork/NeuralNetwork.o"
	@echo "... src/NeuralNetwork/NeuralNetwork.i"
	@echo "... src/NeuralNetwork/NeuralNetwork.s"
	@echo "... src/NeuralNetwork/backPropagation.o"
	@echo "... src/NeuralNetwork/backPropagation.i"
	@echo "... src/NeuralNetwork/backPropagation.s"
	@echo "... src/NeuralNetwork/feedForward.o"
	@echo "... src/NeuralNetwork/feedForward.i"
	@echo "... src/NeuralNetwork/feedForward.s"
	@echo "... src/NeuralNetwork/loadWeights.o"
	@echo "... src/NeuralNetwork/loadWeights.i"
	@echo "... src/NeuralNetwork/loadWeights.s"
	@echo "... src/NeuralNetwork/setErrors.o"
	@echo "... src/NeuralNetwork/setErrors.i"
	@echo "... src/NeuralNetwork/setErrors.s"
	@echo "... src/NeuralNetwork/train.o"
	@echo "... src/NeuralNetwork/train.i"
	@echo "... src/NeuralNetwork/train.s"
	@echo "... src/Neuron.o"
	@echo "... src/Neuron.i"
	@echo "... src/Neuron.s"
	@echo "... src/main.o"
	@echo "... src/main.i"
	@echo "... src/main.s"
	@echo "... src/test_av_matris.o"
	@echo "... src/test_av_matris.i"
	@echo "... src/test_av_matris.s"
	@echo "... src/utils/Math.o"
	@echo "... src/utils/Math.i"
	@echo "... src/utils/Math.s"
	@echo "... src/utils/MatrixToVector.o"
	@echo "... src/utils/MatrixToVector.i"
	@echo "... src/utils/MatrixToVector.s"
	@echo "... src/utils/Misc.o"
	@echo "... src/utils/Misc.i"
	@echo "... src/utils/Misc.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

