/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#include "../include/Matrix.h"

//=================================  
// dependencies
#include <iostream>
#include <algorithm> 
#include <iterator>
#include <exception>
//=================================  
// Namespaces
using namespace std;


//=================================  
// <Implementation>

//================================= 
// <Constructors>
Matrix::Matrix(int rows, int columns, bool randomize_content) 
   :rows{rows}, columns{columns}, matrix{(long unsigned int)rows, vector<double>(columns)}
{
   if (randomize_content)
      for_each(matrix.begin(), matrix.end(), [&] (vector<double>& row) {
            generate(row.begin(), row.end(), [this] () {return getRandomSigmoid();} );
         });
}

Matrix::Matrix(initializer_list< vector<double> > list)
   :rows{(int)list.size()}, columns{(int)std::begin(list)->size()}, matrix{list}
{}

Matrix::Matrix(int rows, int columns, initializer_list<double> list)
   :rows{rows}, columns{columns}, matrix{(long unsigned int)rows, vector<double>(columns)}
{
   if ( rows*columns != (int)list.size()) throw std::length_error("[MATRIX] Matrix::Matrix(int rows, int columns, initializer_list<double> list) row*column != list.size()  \n");
   for_each(matrix.begin(), matrix.end(), [list] (vector<double>& row) {
         std::copy(list.begin(), list.end(), std::back_inserter(row));
      });
}

Matrix::Matrix(Matrix* ptr)
   :rows{ptr->rows}, columns{ptr->columns}, matrix{ptr->matrix}
{}

// </Constructors>  
//================================= 
Matrix *Matrix::copy() {
  Matrix *m = new Matrix(this->rows, this->columns, false);

  for(int i = 0; i < rows; i++) {
    for(int j = 0; j < columns; j++) {
       m->setVal(i, j, this->getVal(i, j));
    }
  }

  return m;
}

Matrix* Matrix::getTranspose() {
   Matrix* m = new Matrix(this->columns, this->rows, false);
   for (int r{}; r < rows;r++) {
      for (int c{}; c < columns; c++) {
         m->setVal(c, r, this->getVal(r, c));
      }
   }
   return m;
}

/** Runtime for transpose: O(k1*log2(n*k2)) , n = iterations, k = instructions in loop**/
void Matrix::transpose() {
   if (rows == columns) {
      for (int r{}; r < rows;r++) {
         for (int c{r+1}; c < columns; c++) {
            swap(matrix[r][c], matrix[c][r]);
         }
      }
   }
   else if (rows > columns) {
      for (int r{}; r < columns;r++) {
         matrix.at(r).resize(rows);
         for (int c{r+1}; c < rows; c++) {
            swap(matrix[r][c], matrix[c][r]);
         }
      }
      matrix.resize(columns);
      swap(this->rows, this->columns);
   }
   else {
      matrix.resize(columns, vector<double>(rows));
      for (int r{}; r < rows;r++) {
         for (int c{r+1}; c < columns; c++) {
            swap(matrix[r][c], matrix[c][r]);
         }
         matrix.at(r).resize(rows);
      }
      swap(this->rows, this->columns);
   }
}



bool Matrix::inRange(int row, int column) const {
   return ( ( 0 <= row && row <= rows) && ( 0 <= column &&  column <= columns));
}

//================================= 
// <Setters> 
void Matrix::setVal(int row, int column, double val) {
#ifdef DEBUG
   if (!inRange(row,column)) throw std::out_of_range("[MATRIX] double Matrix::getVal(...) row || column out of range \n");
#endif
   matrix.at(row).at(column) = val;  
}
// </Setters>
//================================= 

//=================================   
// <Getters> 
// row and colum guaranteed to be in range! No need to use .at() but --\_(^.^)_/--
double Matrix::getVal(int row, int column) const {
#ifdef DEBUG
   if (!inRange(row,column)) throw std::out_of_range("[MATRIX] double Matrix::getVal(...) row || column out of range \n");
#endif
   return matrix.at(row).at(column);
}

double Matrix::getRandomSigmoid() {
   std::random_device rd;
   std::mt19937 gen(rd());
   std::uniform_real_distribution<double> dis(0,1);
   return dis(gen);
}

void Matrix::printToConsole() const {
   for_each(matrix.begin(), matrix.end(), [](const vector<double>& row) {
         cout.precision(2);
         std::copy(row.begin(), row.end(), std::ostream_iterator<double>(cout, "\t")); 
         cout << "\n";
      });
}
// </Getters>  
//================================= 
  
// </Implementation>
//=================================
