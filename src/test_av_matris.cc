/** 
    Author: Anders Vrethem
    Date:   2017-12-28
 **/

//=================================  
// included dependencies
#include <iostream>
#include "../include/Neuron.h"
#include "../include/Matrix.h"

using namespace std;

int main()
{
#ifdef DEBUG
   cout << "\t<<< RUNNING DEBUG BUILD >>>\n"; 
#else
   cout << "Building d\x99rsha's ANN ...\n";
#endif
      
   // Neruon test
   Neuron *n = new Neuron(0.9);
   cout << "Val: " << n->getVal() << endl;
   cout << "ActivatedVal: " << n->getActivatedVal() << endl;
   cout << "Derived Val: " << n->getDerivedVal() << endl;
   
// Matrix test
   Matrix* m = new Matrix({{1,2},{1,2},{1,2},{1,2},{1,2}});
   m->printToConsole();
   m->transpose();
   cout << "Transposed: \n";
   m->printToConsole();
   
   cout << "\n\n Squared matrix\n";
   Matrix* m2 = new Matrix({{1,2,3},{4,5,6}, {7,8,9}});
   m2->printToConsole();
   m2->transpose();
   cout << "Transposed: \n";
   m2->printToConsole();
   
   cout << "\n\n Squared matrix\n";
   delete m2;
   m2 = new Matrix(2,9, true);
   m2->printToConsole();
   m2->transpose();
   cout << "Transposed: \n";
   m2->printToConsole();

   
   return 0;
}
