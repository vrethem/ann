/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#include "../include/Layer.h"

//=================================  
// dependencies
#include "../include/Neuron.h"
#include "../include/Matrix.h"
//=================================  
// Namespaces
using namespace std;


//=================================  
// Implementation - START

//=================================  
// <Constructor>
Layer::Layer(int size) 
   :size{size}, neurons{} 
{
   for( int i{0}; i < size; i++) {
      Neuron* n = new Neuron(0.00);
      this->neurons.push_back(n);
   }
}

Layer::Layer(int size, int activationType) 
   :size{size}, neurons{}
{
  for(int i = 0; i < size; i++) {
    Neuron *n = new Neuron(0.0000000000, activationType);
    this->neurons.push_back(n);
  } 
}
// </Constructor>
//=================================  

Matrix* Layer::matrixify()  {
   Matrix* m = new Matrix(1, this->neurons.size(), false);
   for (int i{}; (unsigned int)i < this->neurons.size(); i++) {
      m->setVal(0,
                i,
                this->neurons.at(i)->getVal());
   }
   return m;
}

Matrix* Layer::matrixifyActivatedVals() {
   Matrix* m = new Matrix(1, this->neurons.size(), false);
   for (int i{}; (unsigned int)i < this->neurons.size(); i++) {
      m->setVal(0,
                i,
                this->neurons.at(i)->getActivatedVal());
   }
   return m;
}

Matrix* Layer::matrixifyDerivedVals() {
   Matrix* m = new Matrix(1, this->neurons.size(), false);
   for (int i{}; (unsigned int)i < this->neurons.size(); i++) {
      m->setVal(0, 
                i, 
                this->neurons.at(i)->getDerivedVal());
   }
   return m;
}

//=================================  
// <Getters>
vector<double> Layer::getActivatedVals() {
  vector<double> ret;

  for(int i = 0; i < (int)this->neurons.size(); i++) {
    double  v = this->neurons.at(i)->getActivatedVal();
    ret.push_back(v);
  }

  return ret;
}

std::vector<Neuron *> Layer::getNeurons() { 
   return this->neurons; 
}
// </Getters>
//================================= 

//=================================  
// <Setters>
void Layer::setNeurons(std::vector<Neuron *> neurons) { 
   this->neurons = neurons; 
}

void Layer::setVal(int i, double v) {
   this->neurons.at(i)->setVal(v);
}
// </Setters>
//================================= 

//=================================  
// Implementation - END
