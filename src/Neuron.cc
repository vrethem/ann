/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#include "../include/Neuron.h"

//=================================  
// dependencies
#include <iostream>
#include <cmath>

//=================================  
// Namespaces
using namespace std;

//=================================  
// Constants
#define TANH 1
#define RELU 2
#define SIGM 3
//=================================  
// Implementation - START

//=================================  
// <Constructor>
Neuron::Neuron(double _val) 
   :val{_val}, activatedVal{}, derivedVal{}, activationType{}
{
   activate();
   derive();
}

Neuron::Neuron(double val, int activationType) 
   :val{val}, activatedVal{}, derivedVal{}, activationType{activationType}
{ 
   activate();
   derive();
}

// </Constructor>
//=================================  

// Fast sigmoid function
// f(x) = x / (1 + |x|)
void Neuron::activate() {
   switch (activationType ) {
   case TANH : activatedVal = tanh(activatedVal);
   case RELU : activatedVal = (val > 0) ? val : 0;
   case SIGM : activatedVal = 1 / (1 + exp(-val));
   default: cout << "[Error]@Neuron::activate() \n\tNone valid default path taken\n"; 
   };
}

// Derivative for fast sigmoid function
// f'(x) = f(x) * (1 - f(x))
void Neuron::derive() {
   switch (activationType ) {
   case TANH : derivedVal = 1.0 - pow(activatedVal,2);
   case RELU : derivedVal = (val > 0) ? 1 : 0;
   case SIGM : derivedVal = activatedVal * (1- activatedVal);
   default: cout << "[Error]@Neuron::activate() \n\tNone valid default path taken\n"; 
   };
}

void Neuron::setVal(double& val) {
   this->val = val;
   activate();
   derive();
}

//=================================  
// Implementation - END
