/** 
    Author: Anders Vrethem
    Date:   2017-12-28
 **/

//=================================  
// included dependencies
#include <iostream>
#include <vector>

//=================================  
// included class dependencies 
#include "../include/Neuron.h"
#include "../include/Matrix.h"
#include "../include/ANN.h"

using namespace std;

int main(int argc, char* argv[])
{
#ifdef DEBUG
   cout << "\t<<< RUNNING DEBUG BUILD >>>\n"; 
#else
   cout << "Building d\x99rsha's ANN ...\n";
#endif
   
   vector<int> topology;
   topology.push_back(3);
   topology.push_back(2);
   topology.push_back(3);
  
   vector<double> input;
   input.push_back(1.0);
   input.push_back(0.0);
   input.push_back(1.0);

   ANN* ann = new ANN(topology);
   ann->setCurrentInput(input);
   ann->print();
   return 0;
}
