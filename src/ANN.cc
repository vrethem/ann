/** 
    Author:        Anders Vrethem
    Last-modified: 2018-01-18
**/
#include "../include/ANN.h"

//=================================  
// dependencies
#include "../include/Matrix.h"
#include "../include/Layer.h"
#include <iostream>
#include <algorithm> 
#include <iterator>
#include <exception>
//=================================  
// Namespaces
using namespace std;


//=================================  
// <Implementation>

// <Constructors>
ANN::ANN(vector<int> topology)
   :topologySize{(int)topology.size()}, topology{topology}, layers{}, weightMatrixes{}, input{}
{
   for (int i{}; i < topologySize; i++) {
      Layer* l = new Layer(topology.at(i));
      layers.push_back(l);
   }
   
   for (int i{}; i < (topologySize - 1); i++) {
      Matrix* m = new Matrix(topology.at(i), topology.at(i + 1), true);
      weightMatrixes.push_back(m);
   }
}
// </Constructors>  
void ANN::setCurrentInput(vector<double> input) {
   this->input = input;
   
   for( int i{}; (unsigned int)i < input.size(); i++) {
      this->layers.at(0)->setVal(i, input.at(i));
   }
}

void ANN::print() {
   for ( int i{}; (unsigned int)i < layers.size();i++) {
      cout << "LAYER_" << i << " ~> ";
      if (i == 0) {
         Matrix *m = this->layers.at(i)->matrixify();;
         m->printToConsole();
      } else {
         Matrix *m = this->layers.at(i)->matrixifyActivatedVals();
         m->printToConsole();
      }
   }
}

// <Setters> 

// </Setters>

  
// <Getters> 

// </Getters>  

  
// </Implementation>
//=================================
