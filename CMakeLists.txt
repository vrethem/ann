cmake_minimum_required(VERSION 3.5)
project(d0rsha_Artificial_Neural_Network)

set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -pedantic -Wall -Wextra -Weffc++ -DDEBUG")
set(CMAKE_INSTALL_MESSAGE "This software is made by d0rsha for fun")
#set(CMAKE_VERBOSE_MAKEFILE true)
include_directories("${PROJECT_SOURCE_DIR}")

# main app
add_executable(
  main
  src/main.cc
  src/Neuron.cc
  src/Layer.cc
  src/Matrix.cc
  src/ANN.cc
  src/NeuralNetwork/backPropagation.cpp
  src/NeuralNetwork/feedForward.cpp
  src/NeuralNetwork/loadWeights.cpp
  src/NeuralNetwork/setErrors.cpp
  src/NeuralNetwork/train.cpp
  src/NeuralNetwork/NeuralNetwork.cpp
  src/utils/Math.cpp
  src/utils/MatrixToVector.cpp
  src/utils/Misc.cpp
)
# Test matrix multiplication
add_executable(
 test_matrix
 src/test_av_matris.cc
 src/Neuron.cc
 src/Layer.cc
 src/Matrix.cc
)

ADD_CUSTOM_TARGET(zap
COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} 
                         --target backupstampfiles
COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR}
                         --target clean
COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR}
                         --target restorestampfiles)
